from django.urls import path
from projects.views import list_all_projects, create_project
from tasks.views import task_display


urlpatterns = [
    path("", list_all_projects, name="list_projects"),
    path("<int:id>/", task_display, name="show_project"),
    path("create/", create_project, name="create_project"),
]
