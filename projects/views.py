from django.shortcuts import render, redirect
from projects.models import Project
from projects.forms import ProjectForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_all_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {"all_projects": list}
    return render(request, "projects/list.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            proj = form.save(False)
            proj.owner = request.user
            proj.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {"new_project": form}
    return render(request, "projects/new_project.html", context)
