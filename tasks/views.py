from django.shortcuts import render, get_object_or_404, redirect
from tasks.models import Task
from projects.models import Project
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def task_display(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/detail.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("home")
    else:
        form = TaskForm()
    context = {
        "new_task": form,
    }
    return render(request, "tasks/new_task.html", context)


@login_required
def my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": tasks}
    return render(request, "tasks/mine.html", context)
